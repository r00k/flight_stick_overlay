﻿; Original Credit to https://github.com/ampersand38/Joystick-Overlay,
; as I'm just modifying their configs on this one

; I control vertical thrust with the toe brakes of my pedals.
; This one was a little tricky as each toe brake is a different axis.
; There's a bit of (simple) math to convert both toe brake axes into a single cursor


#Persistent
#NoEnv
OnExit, GuiClose

;///////////////////////////////////////////////////////////////////////////////
;Configuration
;///////////////////////////////////////////////////////////////////////////////
    
;Use the Joystick Test Script to find desired joyNum and axes letters.
;https://autohotkey.com/docs/scripts/JoystickTest.htm
    
;X Y Z R U V

joyNum := "2"		; Joystick Device number gotten from Joystick Test Script
thrustUp := "U"		; Joystick axis for thrust up gotten from Joystick Test Script
thrustDown := "R"	; Joystick axis for forward and reverse throttle gotten from Joystick Test Script

;Set window position and title.

windowX := "100"	; default horizontal window position from left of main monitor
windowY := "100"	; default vertical window position from top of main monitor
windowTitle := "Vertical Thrust"	; Title of window, used to easily find when importing into OBS

;///////////////////////////////////////////////////////////////////////////////

Gui, Show, W25 H200 X%windowX% Y%windowY%, %windowTitle%	; Draw the window itself, W25 = width of 25 pixels, H200 = height of 200 pixels

W:=25	; determine starting cursor position on X axis of graph
H:=200	; determine starting cursor position on Y axis of graph

SetTimer, Disp, off
DllCall("DeleteObject", "UInt", hPen)
DllCall("DeleteObject", "UInt", hPen2)
DllCall("DeleteObject", "UInt", hPen3)
hPen := DllCall("CreatePen", "UInt", 0, "UInt", 3, "UInt", 0xff00ff)	; Used to draw background border. Set to 0 width and green color (0xff00ff) for chroma key delete
hPen2 := DllCall("CreatePen", "UInt", 0, "UInt", 8, "UInt", 0x000000)	; Moving cursor showing controller input. Set to 8 width and white (0x000000) (creates a sort of diamond shape)
hPen3 := DllCall("CreatePen", "UInt", 0, "UInt", 0, "UInt", 0xff00ff)	; Shows middle point of axes. Anything below 3 width is hard to see on stream. Currently off
hBrush := DllCall("CreateSolidBrush", "UInt", 0xff00ff, "Ptr")	; background color. Set to green for chroma key delete
DllCall("ReleaseDC", "UInt", htx, "UInt", hdcMem)
hdcWin := DllCall("GetDC", "UPtr", hwnd:=WinExist(windowTitle))
hdcMem := DllCall("CreateCompatibleDC", "UPtr", hdcWin, "UPtr")
hbm := DllCall("CreateCompatibleBitmap", "UPtr", hdcWin, "int", W, "int", H, "UPtr")
hbmO := DllCall("SelectObject", "uint", hdcMem, "uint", hbm)
DllCall("SetROP2", "UInt", hdcMem, "UInt", 0x04)	;hex for SRCOPY mix mode

;update rate ~60Hz
SetTimer, Disp, 16
return

;draw and update loop
Disp:
;draw rect to wipe
	DllCall("SelectObject", "UInt", hdcMem, "UInt", hPen)	; select background border pen
	DllCall("SelectObject", "UInt", hdcMem, "UInt", hBrush)	; select background color brush
	DllCall("Rectangle", "UInt", hdcMem, "int", 0 , "int", 0, "int", W, "int", H)

;draw referece
	DllCall("SelectObject", "uint", hdcMem, "uint", hPen3)	; select axes pen
	DllCall("MoveToEx", "UInt", hdcMem, "int", 0, "int", 99, "UInt", NULL)
	DllCall("LineTo", "UInt", hdcMem, "int", W, "int", 99)
	DllCall("MoveToEx", "UInt", hdcMem, "int", 99, "int", 0, "UInt", NULL)
	DllCall("LineTo", "UInt", hdcMem, "int", 99, "int", H)

;read axes from joystick
	x := GetKeyState(joyNum "Joy" thrustUp) * 2	; raw axis value of thrust up from pedals
	y := GetKeyState(joyNum "Joy" thrustDown) * 2	; raw axis value of thrust down from pedals

	vert := 100 - (x/2) + (y/2)	; combine toe brakes into single cursor to display

;draw up thrust
	DllCall("SelectObject", "uint", hdcMem, "uint", hPen2)	; select thrust cursor pen
	DllCall("MoveToEx", "UInt", hdcMem, "int", W-25, "int", vert, "UInt", NULL)	; move thrust cursor pen to joystick value -25 on the X plane
	DllCall("LineTo", "UInt", hdcMem, "int", W, "int", vert)	; Draw line across graph


;update screen
	DllCall("BitBlt", "uint", hdcWin, "int", 0, "int", 0, "int", W, "int", H, "uint", hdcMem, "int", 0, "int", 0, "uint", 0xCC0020)	;hex code for SRCOPY raster-op code
	return
	
ExitSub:
GuiClose:
	DllCall("DeleteObject", "Ptr", hPen)
	DllCall("DeleteObject", "Ptr", hPen2)
	DllCall("DeleteObject", "Ptr", hBrush)
	DllCall("DeleteObject", "Ptr", hbm)
	DllCall("DeleteObject", "Ptr", hbmO)
	DllCall("DeleteDC", "Ptr", hdcMem)
	DllCall("ReleaseDC", "Ptr", hwnd, "UInt", hdcWin)
	ExitApp
