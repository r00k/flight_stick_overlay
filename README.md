# flight_stick_overlay

![Overlay Example](/readme-images/overlay.png) 
 
OBS overlay for my HOSAS + Pedals setup, using https://github.com/ampersand38/Joystick-Overlay as a starting point. I've added comments to most of the areas that may require changes for your setup. 
 
If you have a similar setup to mine, you can follow the steps below. If you're a bit different, you may have to do some experimenting to get what you want.
 
**Step 1** 
 
Download and install [AutoHotkey](https://www.autohotkey.com/) 
 
**Step 2** 
 
Download the [JoystickTest](https://www.autohotkey.com/docs/scripts/JoystickTest.ahk) script. 
 
**Step 3** 
 
Open a scratch pad of some sort so you can take notes. Run the JoystickTest script. You'll see this tooltip follow your mouse around. 
 
![JoystickTest Tooltip](/readme-images/joystick-test.png) 
 
Move your inputs until the values change. Take note of what input maps to which number, and what axes map to which letter. May also help to note what the numerical values of each axis means. 
 
**Step 4** 
 
Edit JoystickTest.ahk and change the value of the JoystickNumber variable towards the top of the script. Increment by one. Repeat Step 3. Continue this process until all of your inputs are documented. 
 
![JoystickTest Script Snippet](/readme-images/joystick-test-script.png) 
 
**Step 5** 
 
Edit the ahk files and modify the Joystick and Axis values at the top of the scripts to match your notes.
